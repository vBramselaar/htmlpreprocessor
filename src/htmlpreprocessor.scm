#!/usr/bin/env -S guile-3.0 --no-auto-compile -e entrée -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (ice-9 ftw)
			 (htmlpreprocessor utils filesystem)
			 (htmlpreprocessor utils logging)
			 (htmlpreprocessor processor))

(define INPUTDIR "./html")
(define OUTPUTDIR "./output")

(define (file-extension? filename extension)
  (cond
   ((or (null? filename) (null? extension))
	#f)
   (else
	(let ((extension-location (- (string-length filename) (string-length extension))))
	  (string=? (substring filename extension-location) extension)))))

(define (process-directory input-directory output-directory)
  (define (proc filename statinfo flag)
	(cond
	 ((and (eq? flag (quote regular)) (file-extension? filename ".html"))
	  (let ((processedFile (process-file filename)))
		(cond
		 ((null? processedFile)
		  (println (make-colour-red "Error with file: " filename))
		  #f)
		 (else
		  (write-file (join-paths output-directory (string-drop filename (string-length input-directory))) processedFile)
		  (println (make-colour-green "Processed file: " filename))
		  #t))))
	 (else
	  #t)))
  (if (access? input-directory R_OK)
	  (ftw input-directory proc)
	  (println (make-colour-red "Can't access directory: " input-directory))))

(define (get-time-difference-in-millis t1 t2)
  (exact->inexact (/ (- (cdr t2) (cdr t1)) 1000)))

(define (entrée args)
  (let ((start-time (gettimeofday)))
	(cond
	 ((= (length args) 1)
	  (process-directory INPUTDIR OUTPUTDIR))
	 ((= (length args) 2)
	  (process-directory (list-ref args 1) OUTPUTDIR))
	 ((= (length args) 3)
	  (process-directory (list-ref args 1) (list-ref args 2))))
	(println "Program took: " (get-time-difference-in-millis start-time (gettimeofday)) " ms")))
