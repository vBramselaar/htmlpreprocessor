;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor processors empty)
  #:use-module (htmlpreprocessor utils logging)
  #:use-module (htmlpreprocessor html records)
  #:export (empty-process))

(define PREFIX "[empty] ")

(define (empty-process filedata workdir htmlelement)
  (println PREFIX "test field with " (htmlelement-attributes htmlelement) " as attributes")
  "")
