;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor processor)
  #:use-module (htmlpreprocessor processors empty)
  #:use-module (htmlpreprocessor processors inserter)
  #:use-module (htmlpreprocessor utils logging)
  #:use-module (htmlpreprocessor utils filesystem)
  #:use-module (htmlpreprocessor html parsing)
  #:use-module (htmlpreprocessor html searching)
  #:use-module (htmlpreprocessor html records)
  #:use-module (htmlpreprocessor html solving)
  #:export (process-file))

(define PREFIX "[processor] ")
(define PROCESSORS (list
					(cons "bvd:empty" empty-process)
					(cons "bvd:insert" inserter-process)))

(define (process-file filepath)
  (cond
   ((not (access? filepath R_OK))
	'())
   (else
	(let* ((text (read-file filepath))
		   (regex-matches (search-all-html text))
		   (match-indices (map regexmatch->index regex-matches))
		   (matches (map regexmatch->string regex-matches))
		   (tags (strings->htmltags matches match-indices))
		   (elements (tags->htmlelements tags text)))
	  (cond
	   ((null? regex-matches)
		text)
	   ((null? elements)
		'())
	   (else
		(process-elements text (dirname filepath) elements)))))))

(define* (process-elements text workdir elements  #:optional (offset 0))
  (cond
   ((null? elements)
	text)
   ((null? text)
	'())
   (else
	(let* ((replacement-text (run-a-processor text workdir (car elements)))
		   (replace-start-index (+ (htmlelement-start (car elements)) offset))
		   (replace-end-index (+ (htmlelement-end (car elements)) offset)))
	  (cond
	   ((null? replacement-text)
		'())
	   (else
		(process-elements (string-replace text replacement-text replace-start-index replace-end-index)
						  workdir
						  (cdr elements)
						  (+ offset (- (string-length replacement-text) (- replace-end-index replace-start-index))))))))))

(define (run-a-processor filedata workdir htmlelement)
  (cond
   ((null? filedata)
	'())
   (else
	(let loop ((current-procs PROCESSORS))
	  (cond
	   ((null? current-procs)
		(println (make-colour-red PREFIX "No processor for keyword: " (htmlelement-type htmlelement)))
		'())
	   ((string=? (car (car current-procs)) (htmlelement-type htmlelement))
		(println PREFIX "Using processor: " (htmlelement-type htmlelement))
		((cdr (car current-procs)) filedata workdir htmlelement))
	   (else (loop (cdr current-procs))))))))
