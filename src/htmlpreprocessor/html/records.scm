;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2022 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor html records)
  #:use-module (srfi srfi-9)
  #:export (make-htmltag
			htmltag-element
			htmltag-attributes-text
			htmltag-selfclosing?
			htmltag-closing?
			htmltag-index
			htmltag-length)
  #:export (make-htmlelement
			htmlelement-type
			htmlelement-attributes
			htmlelement-content
			htmlelement-start
			htmlelement-end))

(define-record-type <htmlelement>
  (make-htmlelement type attributes content start end)
  htmlelement?
  (type        htmlelement-type)
  (attributes  htmlelement-attributes)
  (content     htmlelement-content)
  (start       htmlelement-start)
  (end         htmlelement-end))

(define-record-type <htmltag>
  (make-htmltag element attributes-text selfclosing closing index length)
  htmltag?
  (element         htmltag-element)
  (attributes-text htmltag-attributes-text)
  (selfclosing     htmltag-selfclosing?)
  (closing         htmltag-closing?)
  (index           htmltag-index)
  (length          htmltag-length))
