;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2022 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor html searching)
  #:use-module (htmlpreprocessor utils logging)
  #:use-module (ice-9 regex)
  #:export (search-all-html
			regexmatch->index
			regexmatch->region
			regexmatch->string))

(define html-regex (make-regexp "(<|</)bvd:.*>" regexp/newline))

(define (search-all-html text)
  (list-matches html-regex text))

(define (regexmatch->index match)
  (match:start match))

(define (regexmatch->string match)
  (match:substring match))

(define (regexmatch->region match)
  (cons (match:start match) (match:end match)))
