;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2022 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor html parsing)
  #:use-module (htmlpreprocessor utils logging)
  #:use-module (htmlpreprocessor html records)
  #:use-module (srfi srfi-1)
  #:export (string->htmltag
			strings->htmltags
			string->attributes
			tags-with-content->htmlelement))

(define (strings->htmltags strings indices)
  (cond
   ((or (null? strings) (null? indices))
	'())
   (else
	(let* ((tags (map string->htmltag strings indices))
		   (index-of-null (list-index null? tags)))
	  (cond
	   ((eqv? index-of-null #f)
		tags)
	   (else
		(println (make-colour-red "Error: could not parse " (list-ref strings index-of-null)))
		'()))))))

(define (tags-with-content->htmlelement opening-tag closing-tag content)
  (cond
   ((or (null? content) (null? opening-tag) (null? closing-tag))
	'())
   (else
	(let ((parsed-attributes (string->attributes (htmltag-attributes-text opening-tag)))
		  (element-string (htmltag-element opening-tag))
		  (end-index (+ (htmltag-index closing-tag) (htmltag-length closing-tag))))
	  (cond
	   ((eqv? parsed-attributes #f)
		'())
	   (else
		(make-htmlelement (htmltag-element opening-tag) parsed-attributes content
						  (htmltag-index opening-tag)
						  end-index)))))))

(define (self-closing? tag-string)
  (cond
   ((or (not (string? tag-string)) (< (string-length tag-string) 2))
	'())
   (else
	(string=? "/>" (substring tag-string (- (string-length tag-string) 2))))))

(define (closing-tag? tag-string)
  (cond
   ((or (not (string? tag-string)) (< (string-length tag-string) 2))
	#f)
   (else
	(string=? "</" (substring tag-string 0 2)))))

(define (string->htmltag tag-string index)
  (let* ((closing (closing-tag? tag-string))
		 (start-tag-index (if closing 2 1))
		 (selfclosing (self-closing? tag-string))
		 (end-tag-index (if selfclosing (- (string-length tag-string) 2) (- (string-length tag-string) 1)))
		 (trimmed-tag (string-trim-both (substring tag-string start-tag-index end-tag-index)))
		 (first-space-index (string-index trimmed-tag #\space)))
	(cond
	 ((eqv? first-space-index #f)
	  (make-htmltag trimmed-tag
					"" selfclosing closing index (string-length tag-string)))
	 (else
	  (let* ((element (substring trimmed-tag 0 first-space-index))
			 (attributes-text (substring trimmed-tag (1+ first-space-index) (string-length trimmed-tag))))
		(make-htmltag element attributes-text selfclosing closing index (string-length tag-string)))))))

(define (string->attributes attributes-text)
  (let loop ((next-attribute (parse-next-attribute attributes-text))
			 (attributes '()))
	(cond
	 ((null? next-attribute) attributes)
	 (else
	  (loop (parse-next-attribute (cdr next-attribute)) (append attributes (list (car next-attribute))))))))

(define (parse-next-attribute attributes-text)
  (let ((index-first-attribute (string-index attributes-text #\=)))
	(cond
	 ((eqv? index-first-attribute #f)
	  '())
	 (else
	  (let* ((key (string-trim-both (substring attributes-text 0 index-first-attribute)))
			 (value-end-index (string-index attributes-text #\" (+ index-first-attribute 2)))
			 (value (substring attributes-text (+ index-first-attribute 2) value-end-index)))
		(cons (cons key value) (substring attributes-text (1+ value-end-index))))))))
