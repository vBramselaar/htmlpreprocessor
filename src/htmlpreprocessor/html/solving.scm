;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2022 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor html solving)
  #:use-module (htmlpreprocessor utils logging)
  #:use-module (htmlpreprocessor html records)
  #:use-module (htmlpreprocessor html parsing)
  #:export (tags->htmlelements))

(define (tags->htmlelements tags text)
  (cond
   ((or (null? tags) (null? text))
	'())
   (else
	(resolve-tags text
				  (filter (compose not htmltag-closing?) tags)
				  (filter htmltag-closing? tags)
				  '()))))

(define (resolve-tags text opening-tags closing-tags htmlelements)
  (cond
   ((and (null? opening-tags) (not (null? closing-tags)))
	(println (make-colour-red "Error: dangling closing tags: " closing-tags))
	'())
   ((null? opening-tags)
	htmlelements)
   ((htmltag-selfclosing? (car opening-tags))
	(solve-selfclosing text opening-tags closing-tags htmlelements))
   ((and (not (null? opening-tags)) (null? closing-tags))
	(println (make-colour-red "Error: dangling open tags: " opening-tags))
	'())
   ((string=? (htmltag-element (car opening-tags)) (htmltag-element (car closing-tags)))
	(solve-match text opening-tags closing-tags htmlelements))
   (else
	(println (make-colour-red "Error: opening and closing tags don't match: " (car opening-tags) " " (car closing-tags)))
	'())))

(define (solve-selfclosing text opening-tags closing-tags htmlelements)
  (let ((htmlelement (tags-with-content->htmlelement (car opening-tags) (car opening-tags) "")))
	(cond
	 ((null? htmlelement)
	  (println (make-colour-red "Error: could not parse selfclosing tag to element: " (car opening-tags)))
	  '())
	 (else
	  (resolve-tags text (cdr opening-tags) closing-tags (append htmlelements (list htmlelement)))))))

(define (solve-match text opening-tags closing-tags htmlelements)
  (let* ((content (extract-content text (car opening-tags) (car closing-tags)))
		 (htmlelement (tags-with-content->htmlelement (car opening-tags) (car closing-tags) content)))
	(cond
	 ((null? htmlelement)
	  (println (make-colour-red "Error: could not parse tag to element: " (car opening-tags)))
	  '())
	 (else
	  (resolve-tags text (cdr opening-tags) (cdr closing-tags) (append htmlelements (list htmlelement)))))))

(define (extract-content text opening-tag closing-tag)
  (string-trim-both (substring text (+ (htmltag-index opening-tag) (htmltag-length opening-tag))
							   (htmltag-index closing-tag))))
