;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor utils filesystem)
  #:export (join-paths
			read-file
			write-file))

(define (join-paths . paths)
  (cond
   ((null? paths) '())
   (else
	(let loop ((newPath (string-split (car paths) #\/))
			   (oldPaths (cdr paths)))
	  (cond
	   ((null? oldPaths)
		(let ((final-path (string-join (filter not-string-null? newPath) "/")))
		  (if (string=? (string-take (car paths) 1) "/")
			  (string-append "/" final-path)
			  final-path)))
	   (else
		(loop (append newPath (string-split (car oldPaths) #\/))
			  (cdr oldPaths))))))))

(define (read-file filename)
  (with-input-from-file filename
	(lambda ()
	  (reverse-list->string
	   (let loop ((char (read-char))
				  (result '()))
		 (if (eof-object? char)
			 result
			 (loop (read-char) (cons char result))))))))

(define (write-file filename data)
  (create-path-for-file filename)
  (let ((port (open-file filename "w")))
	(display data port)
	(close-port port)))

(define (create-path-for-file filepath)
  (let loop ((pathList (cdr (string-split filepath #\/)))
			 (currentPath (car (string-split filepath #\/))))
	(cond
	 ((null? pathList) #t)
	 (else
	  (when (not (access? currentPath F_OK))
		(mkdir currentPath))
	  (loop (cdr pathList) (string-append currentPath "/" (car pathList))))) ) )

(define (not-string-null? v)
  (not (string-null? v)))
