;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (htmlpreprocessor utils logging)
  #:export (println
			make-colour-red
			make-colour-green))

(define (println . args)
  (let loop ((remaining-args args))
	(cond
	 ((null? remaining-args)
	  '())
	 ((list? (car remaining-args))
	  (loop (car remaining-args))
	  (loop (cdr remaining-args)))
	 (else
	  (display (car remaining-args))
	  (loop (cdr remaining-args)))))
  (newline))

(define (make-colour-red . args)
  (list "\x1b[1;31m" args "\x1b[0m"))

(define (make-colour-green . args)
  (list "\x1b[1;32m" args "\x1b[0m"))
