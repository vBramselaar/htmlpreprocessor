;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (res custom-runner)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-64))

(define (print-fail runner)
  (display "\x1b[1;31mTEST FAILED\x1b[0m") (newline)
  (format #t "In file: ~a" (test-result-ref runner 'source-file)) (newline)
  (format #t "At line: ~a" (test-result-ref runner 'source-line)) (newline)
  (format #t "Source: ~a" (test-result-ref runner 'source-form)) (newline)
  (format #t "  Expected value: ~a" (test-result-ref runner 'expected-value)) (newline)
  (format #t "    Actual value: ~a" (test-result-ref runner 'actual-value)) (newline)
  (display "------------------------") (newline))

(define (custom-runner)
  (let ((runner (test-runner-null))
		(num-passed 0)
		(num-failed 0))
	(test-runner-on-group-begin! runner
	  (lambda (runner suite-name count)
		(format #t "=== Running ~a ===" suite-name)
		(newline)))
	(test-runner-on-test-end! runner
      (lambda (runner)
		(case (test-result-kind runner)
          ((pass xpass) (set! num-passed (+ num-passed 1)))
          ((fail xfail) (set! num-failed (+ num-failed 1)) (print-fail runner))
		  (else #t))))
	(test-runner-on-final! runner
	  (lambda (runner)
		(format #t "Passing tests: ~d.~%Failing tests: ~d.~%"
				num-passed num-failed)
		(when (= num-failed 0)
		  (display "\x1b[1;32mPASSED\x1b[0m")
		  (newline))))
	runner))

(test-runner-factory
 (lambda () (custom-runner)))
