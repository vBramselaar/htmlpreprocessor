#!/usr/bin/env -S guile --fresh-auto-compile -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(add-to-load-path (dirname (current-filename)))
(add-to-load-path (string-append (dirname (current-filename)) "/../src"))

(use-modules (srfi srfi-64)
			 (res custom-runner)
			 (htmlpreprocessor html records)
			 (htmlpreprocessor html searching)
			 (htmlpreprocessor utils filesystem)
			 (ice-9 regex))

(define (append-my-path path)
  (string-append (dirname (current-filename)) path))

(define html-search-example1 (read-file (append-my-path "/res/insert/sample-data.html")))

(test-begin "htmlsearch-test")
(test-equal (list "<bvd:insert sort=\"asc\" key=\"year\">" "</bvd:insert>" "<bvd:insert sort=\"desc\" key=\"year\">" "</bvd:insert>")
  (map match:substring (search-all-html html-search-example1)))

(test-equal (list 204 254 295 346)
  (map match:start (search-all-html html-search-example1)))

(test-equal (list 238 267 330 359)
  (map match:end (search-all-html html-search-example1)))
(test-end "htmlsearch-test")

(test-begin "htmlregion-test")
(let* ((matches (search-all-html html-search-example1))
	   (first-region (regexmatch->region (car matches))))
  (test-equal first-region (cons 204 238))
  (test-equal (substring html-search-example1 (car first-region) (cdr first-region)) "<bvd:insert sort=\"asc\" key=\"year\">"))
(test-end "htmlregion-test")
