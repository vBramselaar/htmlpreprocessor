#!/usr/bin/env -S guile --fresh-auto-compile -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(add-to-load-path (dirname (current-filename)))

(add-to-load-path (string-append (dirname (current-filename)) "/../src"))
(use-modules (srfi srfi-64)
			 (res custom-runner)
			 (htmlpreprocessor processor)
			 (htmlpreprocessor utils filesystem))

(define (append-my-path path)
  (string-append (dirname (current-filename)) path))

(define empty-expected-result (read-file (append-my-path "/res/empty/sample-data-result.html")))
(define insert-expected-result (read-file (append-my-path "/res/insert/sample-data-result.html")))


(test-begin "process-file-test")
(test-equal empty-expected-result (process-file (append-my-path "/res/empty/sample-data.html")))
(test-equal insert-expected-result (process-file (append-my-path "/res/insert/sample-data.html")))
(test-end "process-file-test")

(test-begin "process-file-faulty-test")
(test-equal '() (process-file (append-my-path "/res/insert/faulty-sample-data.html")))
(test-equal '() (process-file (append-my-path "/res/insert/no-file-found-sample-data.html")))
(test-equal '() (process-file (append-my-path "/file/not/existing/at/all")))
(test-end "process-file-faulty-test")
