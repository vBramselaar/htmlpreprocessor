#!/usr/bin/env -S guile --fresh-auto-compile -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(add-to-load-path (dirname (current-filename)))
(add-to-load-path (string-append (dirname (current-filename)) "/../src"))
(use-modules (srfi srfi-64)
			 (res custom-runner)
			 (htmlpreprocessor utils filesystem))

(test-begin "join-path-test")
(test-equal "/home/me/documents/html/index.html" (join-paths "/home/me/documents/" "/html/index.html"))
(test-equal "/home/me/documents/html/index.html" (join-paths "/home/me/documents" "html/index.html"))
(test-equal "home/me/documents/html/index.html" (join-paths "home/me/documents/" "html/index.html"))
(test-equal "home/me/documents/html/index.html" (join-paths "home/me/documents/" "////html/index.html"))
(test-equal "home/me/documents/html/index.html" (join-paths "home/me/documents////" "/html/index.html"))
(test-equal "/home/me/documents/html/index.html" (join-paths "/home/me/documents////" "html/index.html"))
(test-equal "/home/me/documents/html/index.html" (join-paths "/home/me" "/documents////" "html/index.html"))
(test-end "join-path-test")


(define read-file-expected-result
  "a B c Def gawf wlfja wpajfi waef aiwpf awf
awef awlekfjawfe

awfe alkwjf awfe alkwjf
")

(test-begin "read-file-test")
(test-equal read-file-expected-result (read-file (string-append (dirname (current-filename)) "/res/read-file-sample-data.txt")))
(test-error #t (read-file "./dummy/path/does/not/exist"))
(test-end "read-file-test")
