#!/usr/bin/env -S guile --fresh-auto-compile -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(add-to-load-path (dirname (current-filename)))
(add-to-load-path (string-append (dirname (current-filename)) "/../src"))

(use-modules (srfi srfi-64)
			 (res custom-runner)
			 (htmlpreprocessor html records)
			 (htmlpreprocessor html solving))

(define happyflow-text "<div></div>")
(define happyflow-tags (list (make-htmltag "div" "" #f #f 0 5)
							 (make-htmltag "div" "" #f #t 5 5)))
(define happyflow-expected (list (make-htmlelement "div" '() "" 0 10)))

(define selfclosing-text "<h1/>\n
<div></div>
<aselfclosingtag param=\"test\" />")
(define selfclosing-tags (list (make-htmltag "h1" "" #t #f 0 5)
							   (make-htmltag "div" "" #f #f 6 5)
							   (make-htmltag "div" "" #f #t 11 6)
							   (make-htmltag "aselfclosingtag" "param=\"test\"" #t #f 17 32)))
(define selfclosing-expected (list (make-htmlelement "h1" '() "" 0 5)
								   (make-htmlelement "div" '() "" 6 17)
								   (make-htmlelement "aselfclosingtag" (list (cons "param" "test")) "" 17 49)))

(define withcontent-text "<h1/>\n
<div param1=\"test1\" param2=\"test2\">This is the content of this div</div>")
(define withcontent-tags (list (make-htmltag "h1" "" #t #f 0 5)
							   (make-htmltag "div" "param1=\"test1\" param2=\"test2\"" #f #f 6 36)
							   (make-htmltag "div" "" #f #t 73 6)))
(define withcontent-expected (list (make-htmlelement "h1" '() "" 0 5)
								   (make-htmlelement "div" (list (cons "param1" "test1") (cons "param2" "test2")) "This is the content of this div" 6 79)))

(define missingopening-text "</h1>
<div>This is the content of this div</div>")
(define missingopening-tags (list (make-htmltag "h1" "" #f #t 0 5)
							   (make-htmltag "div" "" #f #f 5 5)
							   (make-htmltag "div" "" #f #t 42 6)))
(define missingopening-expected '())

(define missingclosing-text "<h1>
<div>This is the content of this div</div>")
(define missingclosing-tags (list (make-htmltag "h1" "" #f #f 0 4)
							   (make-htmltag "div" "" #f #f 4 5)
							   (make-htmltag "div" "" #f #t 41 6)))
(define missingclosing-expected '())

(test-begin "htmlsolving-test")
(test-equal happyflow-expected (tags->htmlelements happyflow-tags happyflow-text))
(test-equal selfclosing-expected (tags->htmlelements selfclosing-tags selfclosing-text))
(test-equal withcontent-expected (tags->htmlelements withcontent-tags withcontent-text))
(test-equal missingopening-expected (tags->htmlelements missingopening-tags missingopening-text))
(test-equal missingclosing-expected (tags->htmlelements missingclosing-tags missingclosing-text))
(test-end "htmlsolving-test")
