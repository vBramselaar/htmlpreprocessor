#!/usr/bin/env -S guile --fresh-auto-compile -s
!#
;; This file is part of HtmlPreprocessor.
;; Copyright (C) 2021 Bram van Donselaar

;; HtmlPreprocessor is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(add-to-load-path (dirname (current-filename)))
(add-to-load-path (string-append (dirname (current-filename)) "/../src"))

(use-modules (srfi srfi-64)
			 (res custom-runner)
			 (htmlpreprocessor html records)
			 (htmlpreprocessor html parsing))

(test-begin "htmltag-test")
(test-equal (make-htmltag "div" "" #f #f 0 5)  (string->htmltag "<div>" 0))
(test-equal (make-htmltag "div" "" #f #t 0 6)  (string->htmltag "</div>" 0))
(test-equal (make-htmltag "div" "" #t #f 0 6)  (string->htmltag "<div/>" 0))
(test-equal (make-htmltag "div" "id=\"test\"" #f #f 0 15)  (string->htmltag "<div id=\"test\">" 0))
(test-equal (make-htmltag "div" "id=\"test\"" #t #f 0 16)  (string->htmltag "<div id=\"test\"/>" 0))
(test-equal (make-htmltag "div" "id=\"test\" sort=\"true\"" #f #f 0 28)  (string->htmltag "<div id=\"test\" sort=\"true\" >" 0))
(test-equal (make-htmltag "div" "id=\"tes t\" sort=\"tr ue\"" #f #f 0 30)  (string->htmltag "<div id=\"tes t\" sort=\"tr ue\" >" 0))
(test-equal (make-htmltag "div" "id=\"tes t\" sort=\"tr ue\"" #f #f 0 29)  (string->htmltag "<div id=\"tes t\" sort=\"tr ue\">" 0))
(test-end "htmltag-test")

(test-begin "htmlattributes-test")
(test-equal (list (cons "id" "test")) (string->attributes "id=\"test\""))
(test-equal (list (cons "id" "test") (cons "sort" "true")) (string->attributes "id=\"test\" sort=\"true\""))
(test-equal (list (cons "id" "test") (cons "sort" "true") (cons "arg" "val")) (string->attributes "id=\"test\" sort=\"true\" arg=\"val\""))
(test-equal (list (cons "id" "te st") (cons "sort" "tr ue")) (string->attributes "id=\"te st\" sort=\"tr ue\""))
(test-equal (list (cons "id" "te st") (cons "sort" "tr ue ")) (string->attributes " id=\"te st\" sort=\"tr ue \" "))
(test-end "htmlattributes-test")

(test-begin "htmlelement-test")
(define htmlelementTestText1 "<div>test</div>")
(define htmlelementTestText2 "<div id=\"test\" sort=\"true\" > test </div>")

(let ((opening-tag (make-htmltag "div" "" #f #f 0 5))
	  (closing-tag (make-htmltag "div" "" #f #t 9 6)))
  (test-equal "div" (htmltag-element closing-tag))
  (test-equal (make-htmlelement "div" '() "test" 0 15) (tags-with-content->htmlelement opening-tag closing-tag "test")))

(let ((opening-tag (make-htmltag "div" "id=\"test\" sort=\"true\"" #f #f 0 28))
	  (closing-tag (make-htmltag "div" "" #f #t 34 7)))
  (test-equal (make-htmlelement "div" (list (cons "id" "test") (cons "sort" "true")) "test" 0 41)
	(tags-with-content->htmlelement opening-tag closing-tag "test")))
(test-end "htmlelement-test")
