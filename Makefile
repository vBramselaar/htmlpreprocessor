PRG:=htmlpreprocessor

GUILE:=guild
GUILEFLAGS:=--load-path=./src
DESTDIR:=
SITEPATH:=/usr/share/guile/site/3.0
SITECACHEPATH:=/usr/lib/x86_64-linux-gnu/guile/3.0/site-ccache
BINPATH:=/usr/bin

MAINSRC:=./src/$(PRG).scm
MAINOBJ:=./src/$(PRG).go
SRC:= $(wildcard src/htmlpreprocessor/*.scm) \
	$(wildcard src/htmlpreprocessor/processors/*.scm) \
	$(wildcard src/htmlpreprocessor/html/*.scm) \
	$(wildcard src/htmlpreprocessor/utils/*.scm)
DESTSRC:=$(patsubst src/%,$(SITEPATH)/%, $(SRC))

OBJ:=$(patsubst %.scm,%.go, $(SRC))
DESTOBJ:=$(patsubst src/%,$(SITECACHEPATH)/%, $(OBJ))

.PHONY:all clean

all:$(PRG)

$(PRG):$(OBJ) $(MAINOBJ)
	@echo "Build complete"

%.go:%.scm
	$(GUILE) compile $(GUILEFLAGS) $< -o $@

install:$(DESTSRC) $(DESTOBJ)
	mkdir -p $(DESTDIR)$(BINPATH)
	cp $(MAINSRC) $(DESTDIR)$(BINPATH)/$(PRG)
	@echo "Install complete"

$(SITEPATH)/%.scm:src/%.scm
	mkdir -p $(DESTDIR)$(dir $@)
	cp $< $(DESTDIR)$@

$(SITECACHEPATH)/%.go:src/%.go
	mkdir -p $(DESTDIR)$(dir $@)
	cp $< $(DESTDIR)$@

clean:
	rm -rf $(OBJ)
	rm -f $(MAINOBJ)
